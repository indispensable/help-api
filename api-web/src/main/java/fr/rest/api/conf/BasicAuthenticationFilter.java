package fr.rest.api.conf;

import fr.rest.api.exception.HelpException;
import fr.rest.api.resources.model.AccountDto;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.StringTokenizer;

@Provider
public class BasicAuthenticationFilter implements ContainerRequestFilter {

    private static final String SAGGER_JSON = "swagger.json";
    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(BasicAuthenticationFilter.class);
    private static final String AUTHENTIFICATION_SCHEME = "Basic";

    private String username = "";

    private String password = "";

    private String realm = "Protected";

    @Context
    private ResourceInfo resourceInfo;

    private static final HelpException accessDeniedError;

    private static final HelpException accessFrobidenError;

    static {
        accessDeniedError = new HelpException(Response.Status.UNAUTHORIZED.name(), "AUTHENTIFICATION", "You cant access this resources");
        accessFrobidenError = new HelpException(Response.Status.FORBIDDEN.name(), "AUTHENTIFICATION", "Acces block for all users");
    }

    private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED).entity(accessDeniedError).build();
    private static final Response ACCESS_FORBIDEN = Response.status(Response.Status.FORBIDDEN).entity(accessDeniedError).build();
    private String applicativeUser = "Amarauser";
    private String applicativePassword = "password";

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String uri = requestContext.getUriInfo().getPath();
        if (!SAGGER_JSON.equalsIgnoreCase(uri)) {
            Method method = resourceInfo.getResourceMethod();
            // access all
            if (!method.isAnnotationPresent(PermitAll.class)) {

                if (method.isAnnotationPresent(DenyAll.class)) {
                    requestContext.abortWith(ACCESS_FORBIDEN);
                    return;
                }
                // Get request header
                final MultivaluedMap<String, String> headers = requestContext.getHeaders();

                List<String> authorization = headers.get("Authorization");

                if (authorization == null || authorization.isEmpty()) {
                    requestContext.abortWith(ACCESS_DENIED);
                    LOG.error("Authentification denied for user name {}, empty information present in header about authentification.",
                            applicativeUser);
                    return;
                }
                checkAuthorization(requestContext, method, authorization);

            }
        }
    }

    private void checkAuthorization(ContainerRequestContext requestContext, Method method, List<String> authorization) {

        // get encoded user name and passeword.
        final String enodedUserNameAndPassword = authorization.get(0).replaceFirst(AUTHENTIFICATION_SCHEME + " ", "");
        String userNameAndPass = new String(Base64.decodeBase64(enodedUserNameAndPassword.getBytes()));

        // Split user name and passeword.
        final StringTokenizer tokenizer = new StringTokenizer(userNameAndPass, ":");
        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();

        // verify
        if (method.isAnnotationPresent(RolesAllowed.class) || isUserAllowed(username, password)) {
            LOG.info("Authentification not allowed for username {}", username);
            requestContext.abortWith(ACCESS_DENIED);
            return;
        }

    }

    private boolean isUserAllowed(String username, String password) {
        if (applicativeUser.equalsIgnoreCase(username) && applicativePassword.equalsIgnoreCase(password)) {
            LOG.info("Authentification is allowed for username {}", username);
            return true;
        }
        return false;
    }
}