package fr.rest.api.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import fr.core.config.CoreConfiguration;
import fr.rest.api.resources.rs.account.AccountResources;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.swagger.Swagger2Feature;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.RuntimeDelegate;
import java.util.Arrays;

@Configuration
@Import(CoreConfiguration.class)
@ComponentScan(basePackages = "fr.rest.api")
public class ApplicationConfig {

    @Autowired
    private AccountResources accountResources;

    @ApplicationPath("/help")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public class HelpRsApiApplication extends Application {
    }

    @Bean(destroyMethod = "shutdown")
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    @DependsOn("cxf")
    public Server jaxRsServer() {
        JAXRSServerFactoryBean factory = RuntimeDelegate.getInstance().createEndpoint(helpRsApiApplication(), JAXRSServerFactoryBean.class);
        factory.setServiceBean(Arrays.asList(accountResources));
        factory.setAddress("");
        factory.setProviders(Arrays.asList(jsonProvider(), corsFilter(), basicAuthenticationFilter()));
        factory.setFeatures(Arrays.asList(new LoggingFeature(), swagger2Feature()));
        return factory.create();
    }

    public BasicAuthenticationFilter basicAuthenticationFilter(){
        return new BasicAuthenticationFilter();
    }


    @Bean
    public Swagger2Feature swagger2Feature() {
        Swagger2Feature feature = new Swagger2Feature();
        feature.setVersion(getClass().getPackage().getImplementationVersion());
        feature.setContact("tafni.amar@gmail.com");
        feature.setDescription("rest api with cxf implementation");
        feature.setLicense("www.amara.tafni.com");
        //feature.setScanAllResources(true);// generate One UserApi for all resources.
        feature.setBasePath("/help");
        return feature;
    }

    @Bean
    CrossOriginResourceSharingFilter corsFilter() {
        return new CrossOriginResourceSharingFilter();
    }

    @Bean
    ObjectMapper jacksonMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());
        return new ObjectMapper();
    }

    @Bean
    JacksonJsonProvider jsonProvider() {
        JacksonJsonProvider jacksonJsonProvider = new JacksonJsonProvider();
        jacksonJsonProvider.setMapper(jacksonMapper());
        ObjectMapper mapper = new ObjectMapper();
        return new JacksonJsonProvider(mapper);
    }

    @Bean
    HelpRsApiApplication helpRsApiApplication() {
        return new HelpRsApiApplication();
    }
}
