package fr.rest.api.conf;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApplicationInitializer implements WebApplicationInitializer {

    private static final String CONFIG_LOCATION = "fr.rest.api.conf";
    private static final String MAPPING_URL = "/help/*";

    public void onStartup(ServletContext servletContext) throws ServletException {
        servletContext.setInitParameter("contextConfigLocation", CONFIG_LOCATION);
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        if (rootContext != null) {
            servletContext.addListener(new ContextLoaderListener(rootContext));
        }
        CXFServlet cxfServlet = new CXFServlet();
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("CXFServlet", cxfServlet);
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping(MAPPING_URL);
    }
}