package fr.rest.api.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SampleExceptionMapper implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception exception) {
        return null;
    }

   /* public Response toResponse(Exception exception) {
        if (exception instanceof HelpException) {
            HelpException e = (HelpException) exception;
            return Response.status(Response.Status.NOT_FOUND).entity("").build();
        } else {
            BusinessException e = (BusinessException) exception;
            return Response.status(500).entity("").type("application/json").build();
        }
    }*/
}