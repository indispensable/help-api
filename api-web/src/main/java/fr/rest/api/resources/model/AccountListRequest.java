package fr.rest.api.resources.model;

import fr.rest.api.resources.model.AccountDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "AccountListRequest", description = "Account List Request")
public class AccountListRequest {

    List<AccountDto> accounts;

    @ApiModelProperty(position = 2, required = true, value = "List of accounts")
    public List<AccountDto> getAccounts() {
        return accounts;
    }
}
