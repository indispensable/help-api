package fr.rest.api.resources.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "AccountRequest", description = "Account Request")
public class AccountRequest {

    private String accountNumber;
    private String status;

    @ApiModelProperty(position = 1, required = true, value = "account number")
    public String getAccountNumber() {
        return accountNumber;
    }

    @ApiModelProperty(position = 2, required = true, value = "Status of account number")
    public String getStatus() {
        return status;
    }
}
