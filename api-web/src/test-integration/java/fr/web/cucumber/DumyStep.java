package fr.web.cucumber;

import org.springframework.test.context.ContextConfiguration;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.rest.api.conf.ApplicationConfig;

@ContextConfiguration(classes = { ApplicationConfig.class })
public class DumyStep {

	@Given("^there are 1 coffees left in the machine$")
	public void there_are_1_coffees_left_in_the_machine() {
	}

	@And("^I have deposited 1\\$$")
	public void i_have_deposited_1() {
	}

	@When("^I press the coffee button$")
	public void i_press_the_coffee_button() {
	}

	@Then("^I should be served a coffee$")
	public void i_should_be_served_a_coffee() {
	}

}
