package fr.core.dao.transaction;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import fr.core.business.AuthorizationStatus;
import fr.core.business.AuthorizationType;
import fr.core.business.TransactionType;
import fr.core.dao.model.TransactionEntity;

@Repository
public class DefaultTransactionDao implements TransactionDao {

	@Autowired
	private MappingManager mappingManager;

	@Override
	public void save(String accountNumber, String hashCard, String transactionId, AuthorizationType authorizationType,
			AuthorizationStatus authorizationStatus, TransactionType transactionType, BigDecimal amount,
			String holdAction) {
		Mapper<TransactionEntity> transactionMapper = mappingManager.mapper(TransactionEntity.class);
		transactionMapper.save(new TransactionEntity(transactionId, accountNumber, hashCard, authorizationType.name(),
				authorizationStatus.name(), amount, holdAction, false));
	}

}
