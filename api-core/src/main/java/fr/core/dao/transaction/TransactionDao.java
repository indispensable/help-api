package fr.core.dao.transaction;

import java.math.BigDecimal;

import fr.core.business.AuthorizationStatus;
import fr.core.business.AuthorizationType;
import fr.core.business.TransactionType;

public interface TransactionDao {

	void save(String accountNumber, String hashCard, String transactionId, AuthorizationType authorizationType,
			AuthorizationStatus authorizationStatus, TransactionType transactionType, BigDecimal amount,
			String holdAction);
}
