package fr.core.dao.model;

import java.math.BigDecimal;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;

@Table(name = "ACCOUNT_BALANCE")
public class AccountBalanceEntity {

	@Column(name = "ACCOUNT_NUMBER")
	@PartitionKey
	private final String accountNumber;

	@Column(name = "BALANCE")
	private final BigDecimal balance;

	public AccountBalanceEntity() {
		this(null,null);
	}
	public AccountBalanceEntity(String accountNumber, BigDecimal balance) {
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

}
