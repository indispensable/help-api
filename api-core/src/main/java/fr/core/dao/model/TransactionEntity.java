package fr.core.dao.model;

import java.math.BigDecimal;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;

@Table(name = "TRANSACTION")
public final class TransactionEntity {

	@Column(name = "TRANSACTION_ID")
	private final String id;

	@Column(name = "ACCOUNT_NUMBER")
	private final String accountNumber;

	@Column(name = "HASH_CARD")
	private final String hashCard;

	@Column(name = "AUTHORIZATION_TYPE")
	private final String authorizationType;

	@Column(name = "STATUS")
	private final String status;

	@Column(name = "AMOUNT")
	private final BigDecimal amount;

	@Column(name = "HOLD_ACTION")
	private final String holdAction;

	@Column(name = "CANCEL")
	private final boolean cancel;

	private TransactionEntity() {
		this(null, null, null, null, null, null, null, false);
	}

	public TransactionEntity(String id, String accountNumber, String hashCard, String authorizationType, String status,
			BigDecimal amount, String holdAction, boolean cancel) {
		this.id = id;
		this.accountNumber = accountNumber;
		this.hashCard = hashCard;
		this.authorizationType = authorizationType;
		this.status = status;
		this.amount = amount;
		this.holdAction = holdAction;
		this.cancel = cancel;
	}

	public String getId() {
		return id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getHashCard() {
		return hashCard;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getHoldAction() {
		return holdAction;
	}

	public String getStatus() {
		return status;
	}

	public boolean isCancel() {
		return cancel;
	}

}
