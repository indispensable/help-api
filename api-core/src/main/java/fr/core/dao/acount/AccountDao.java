package fr.core.dao.acount;

import java.math.BigDecimal;

import fr.core.dao.model.AccountBalanceEntity;

public interface AccountDao {
	/**
	 * 
	 * @param hashCode
	 * @return
	 */
	public AccountBalanceEntity findBalance(String accountNumber);

	/**
	 * 
	 * @param accountNumber
	 * @param hashCard TODO
	 * @return
	 */
	BigDecimal findHold(String accountNumber, String hashCard);
}
