package fr.core.dao.acount;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.datastax.driver.mapping.MappingManager;

import fr.core.dao.model.AccountBalanceEntity;

@Repository
public class DefaultAccountDao implements AccountDao {

	@Autowired
	private MappingManager cassandraManager;

	@Override
	public AccountBalanceEntity findBalance(String accountNumber) {
		return cassandraManager.mapper(AccountBalanceEntity.class).get(accountNumber);
	}

	@Override
	public BigDecimal findHold(String accountNumber, String hashCard) {
		return new BigDecimal(0);
	}
}
