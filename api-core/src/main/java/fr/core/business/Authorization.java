package fr.core.business;

/**
 * Banking authorization.
 */
public final class Authorization {

	private final String transactionId;

	private final String hashCard;

	private final Account account;

	private final AuthorizationType type;

	private final AuthorizationStatus status;

	public Authorization(String transactionId, String hashCard, Account account, AuthorizationType type, AuthorizationStatus status) {
		this.transactionId = transactionId;
		this.hashCard = hashCard;
		this.account = account;
		this.type = type;
		this.status = status;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getHashCard() {
		return hashCard;
	}

	public Account getAccount() {
		return account;
	}

	public AuthorizationType getType() {
		return type;
	}

	public AuthorizationStatus getStatus() {
		return status;
	}

}
