package fr.core.business;

public enum CardType {
	
	DIRECT_DEBIT, DEFERRED_DEBIT
}
