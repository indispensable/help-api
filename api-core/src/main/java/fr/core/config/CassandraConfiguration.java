package fr.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;

@Configuration
public class CassandraConfiguration {

	private static final String KEYSPACE = "help_keyspace";
	private static final String CONTACT_POINT = "127.0.0.1";

	@Bean
	public MappingManager cassandraManager() {
		Cluster cluster = Cluster.builder().addContactPoints(CONTACT_POINT).withPort(9142).build();
		Session session = cluster.connect(KEYSPACE);
		return new MappingManager(session);
	}
}
