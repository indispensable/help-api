package fr.core.services.account;

import fr.core.business.Account;
import fr.core.exception.AccountBusinessException;

public interface AccountService {
	/**
	 * Find account details.
	 *
	 * @param accountNumber
	 * @return account details.
	 * @throws AccountBusinessException
	 */
	Account findDetails(String accountNumber) throws AccountBusinessException;

	/**
	 * 
	 * @param hashCode
	 * @return
	 */
	Account findRealBalance(String accountNumber, String hashCode);

}
