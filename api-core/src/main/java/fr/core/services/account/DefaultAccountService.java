package fr.core.services.account;

import fr.core.business.Account;
import fr.core.dao.acount.AccountDao;
import fr.core.dao.model.AccountBalanceEntity;
import fr.core.exception.AccountBusinessException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static fr.core.exception.AccountBusinessException.AccountBusinessError;

import java.math.BigDecimal;

@Service
public class DefaultAccountService implements AccountService {

	@Autowired
	private AccountDao accountDao;

	@SuppressWarnings("unused")
	@Override
	public Account findDetails(String accountNumber) throws AccountBusinessException {
		Account account = new Account("", new BigDecimal(300));
		if (account == null) {
			// A functional error. i can return account not found.
			// for example i can display a message.
			throw new AccountBusinessException(AccountBusinessError.ACCOUNT_NOT_FOUND, "account not exist",
					"Account Domain");
		}
		return null;
	}

	@Override
	public Account findRealBalance(String accountNumber, String hashCard) {
		// basic balance
		AccountBalanceEntity accountBalanceEntity = accountDao.findBalance(accountNumber);
		// hold 
		BigDecimal hold = accountDao.findHold(accountNumber, hashCard);
		// 
		BigDecimal realBalance = accountBalanceEntity.getBalance().add(hold);
		return new Account(accountNumber, realBalance);
	}
}
