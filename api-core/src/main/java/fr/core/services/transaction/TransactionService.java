package fr.core.services.transaction;

import java.math.BigDecimal;

import fr.core.business.Authorization;
import fr.core.business.AuthorizationType;
import fr.core.business.TransactionType;

public interface TransactionService {

	Authorization authorize(String accountNumber, String hashCard, String transactionId, AuthorizationType authorizationType,
			TransactionType transactionType, BigDecimal amount);

}
