package fr.core.services.transaction;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.core.business.Account;
import fr.core.business.Authorization;
import fr.core.business.AuthorizationStatus;
import fr.core.business.AuthorizationType;
import fr.core.business.TransactionType;
import fr.core.dao.transaction.TransactionDao;
import fr.core.services.account.AccountService;

@Service
public class DefaultTransactionService implements TransactionService {

	@Autowired
	private AccountService accountService;

	@Autowired
	private TransactionDao transactionDao;

	@Override
	public Authorization authorize(String accountNumber, String hashCard, String transactionId,
			AuthorizationType authorizationType, TransactionType transactionType, BigDecimal amount) {

		Account account = accountService.findRealBalance(accountNumber, hashCard);

		if (account.getBalance().compareTo(amount) < 0) {
			transactionDao.save(accountNumber, hashCard, transactionId, authorizationType, AuthorizationStatus.DECLINED,
					transactionType, amount, null);
			// TODO
			return new Authorization(transactionId, hashCard, account, authorizationType, AuthorizationStatus.DECLINED);
		}

		transactionDao.save(accountNumber, hashCard, transactionId, authorizationType, AuthorizationStatus.ACCEPT,
				transactionType, amount, postHold(authorizationType, transactionType) ? "TO_BE_HELD" : null);
		return new Authorization(transactionId, hashCard, account, authorizationType, AuthorizationStatus.ACCEPT);

	}

	private boolean postHold(AuthorizationType authorizationType, TransactionType transactionType) {
		return false;
	}
}
