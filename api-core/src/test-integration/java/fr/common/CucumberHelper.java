package fr.common;

import java.io.IOException;

import org.apache.cassandra.exceptions.ConfigurationException;
import org.apache.thrift.transport.TTransportException;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.BeforeClass;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class CucumberHelper {

	public static final String KEYSPACE_CREATION_QUERY = "CREATE KEYSPACE IF NOT EXISTS help_keyspace "
			+ "WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '3' };";

	public static final String KEYSPACE_ACTIVATE_QUERY = "USE help_keyspace;";

//	private static final String SQL_INSERT_1 = "INSERT INTO TRANSACTION VALUES ('t1','TO_BE_HOLDED','accpet','false','12ZSA4HGFFDR565444', '400','12345')";
//
//	private static final String SQL_INSERT_2 = "INSERT INTO ACCOUNT_BALANCE VALUES ('12345', '400')";

	@BeforeClass
	public static void startCassandraEmbedded()
			throws InterruptedException, TTransportException, ConfigurationException, IOException {
		EmbeddedCassandraServerHelper.startEmbeddedCassandra();
		final Cluster cluster = Cluster.builder().addContactPoints("127.0.0.1").withPort(9142).build();
		final Session session = cluster.connect();
		session.execute(KEYSPACE_CREATION_QUERY);
		session.execute(KEYSPACE_ACTIVATE_QUERY);
		session.execute(
				"CREATE TABLE ACCOUNT_BALANCE(ACCOUNT_NUMBER TEXT, BALANCE decimal, PRIMARY KEY(ACCOUNT_NUMBER));");
		session.execute(
				"CREATE TABLE TRANSACTION(TRANSACTION_ID text, HOLD_ACTION text, STATUS text, CANCEL boolean, HASH_CARD text, AMOUNT Decimal, ACCOUNT_NUMBER text, authorization_type text, PRIMARY KEY (transaction_id));");
		//session.execute(SQL_INSERT_1);
		Thread.sleep(5000);
	}
}
