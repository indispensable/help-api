package fr.core.services.transaction;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import fr.common.CucumberHelper;

@RunWith(Cucumber.class)
@CucumberOptions(format = { "pretty", "html:target/cucumber" })
public class TransactionServiceIT extends CucumberHelper {

}
