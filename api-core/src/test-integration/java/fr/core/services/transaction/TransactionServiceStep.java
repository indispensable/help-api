package fr.core.services.transaction;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.core.business.Authorization;
import fr.core.business.AuthorizationType;
import fr.core.business.TransactionType;
import fr.core.config.CoreConfiguration;
import fr.core.dao.model.AccountBalanceEntity;

@ContextConfiguration(classes = { CoreConfiguration.class })
public class TransactionServiceStep {

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private MappingManager manager;

	private BigDecimal balance;

	private Authorization authorization;

	@Given("^a checking account with balance (.+)$")
	public void a_checking_account_with_balance(BigDecimal balanceParam) {
		balance = balanceParam;
		insertBalance(balanceParam);
	}

	@When("^i withdrawal (.+)$")
	public void i_withdrawal(BigDecimal amount) {
		authorization = transactionService.authorize("12345", "12ZSA4HGFFDR565444", "t1", AuthorizationType.WITHDRAWAL,
				TransactionType.TODO, amount);
	}

	@Then("^a transaction should be (.+)$")
	public void a_transaction_should_be(String status) {
		assertThat(authorization).isNotNull();
		assertThat(balance).isEqualTo(authorization.getAccount().getBalance());
		assertThat(authorization.getStatus().name()).isEqualTo(status);
	}

	private void insertBalance(BigDecimal balanceParam) {
		Mapper<AccountBalanceEntity> mapper2 = manager.mapper(AccountBalanceEntity.class);
		mapper2.save(new AccountBalanceEntity("12345", balanceParam));
	}
}
