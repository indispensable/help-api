package fr.cucumber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.core.config.CoreConfiguration;
import fr.core.exception.AccountBusinessException;
import fr.core.services.account.AccountService;

@ContextConfiguration(classes = { CoreConfiguration.class })
public class DumyStep {

	@Autowired
	AccountService accountService;

	@Given("^the seller has '(\\d+)' apples to sell$")
	public void the_seller_has_apples_to_sell(int availableApples) {
	}

	@When("^the customer asks for '(\\d+)' apple$")
	public void the_customer_asks_for_apple(int amountApplesAsked) throws AccountBusinessException {
		accountService.findDetails("12233");
	}

	@Then("^the customer gets '(\\d+)' apple from the seller$")
	public void the_customer_gets_apple_from_the_seller(int amountApplesAsked) {
	}

	@Then("^the customer gets no apple from the seller$")
	public void the_customer_gets_no_apple_from_the_seller() throws Throwable {

	}

}
