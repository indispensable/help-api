Feature: Authorize a new transaction
  As card from HUB

  Scenario Outline: authorize transaction
    Given a checking account with balance <balance>
    When i withdrawal <amount>
    Then a transaction should be <status>

    Examples: 
      | balance | amount | status   |
      | 100     | 50     | ACCEPT   |
      | 200     | 300    | DECLINED |
